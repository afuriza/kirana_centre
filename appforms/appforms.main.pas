unit AppForms.Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  ExtCtrls, SnapUtils,
  AppForms.AppCentre;

type

  { TMainForm }

  TMainForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    labelStartup: TLabel;
    ProgressBar1: TProgressBar;
    TimerStart: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerStartTimer(Sender: TObject);
  private
    procedure OnTaskDone(Sender: TObject);
  public

  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);
begin

end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin

end;

procedure TMainForm.OnTaskDone(Sender: TObject);
begin
  labelStartup.Caption := 'Getting ' + SnapPackages.ResponseData.Count.ToString +
    ' line(s) of data...';
  TimerStart.Enabled := True;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  SnapPackages.OnTaskDone := @OnTaskDone;
  SnapPackages.StartTask(URI_FIND);
end;

procedure TMainForm.TimerStartTimer(Sender: TObject);
begin
  TimerStart.Enabled := False;
  Self.Hide;
  AppCentreForm.Show;
end;

end.

