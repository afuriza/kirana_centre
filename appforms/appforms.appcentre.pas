unit AppForms.AppCentre;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  ExtCtrls, Buttons, SnapUtils, jsonparser, fpjson,
  SnapUtils.DownloadFile;

type

  { TAppCentreForm }

  TAppCentreForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    PackageIcons: TImageList;
    LbStatus: TLabel;
    BtnInstall: TButton;
    LvPackages: TListView;
    Panel1: TPanel;
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure LvPackagesSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
  private
    IconIndex: integer;
    procedure GetPackageIcon;
    procedure AddDefaultIcon;
    procedure OnDownloadComplete(Sender: TObject);
    procedure OnDownloadException(Sender: TObject);
  public

  end;

var
  AppCentreForm: TAppCentreForm;

implementation

{$R *.lfm}

{ TAppCentreForm }

procedure TAppCentreForm.GetPackageIcon;
var
  downloader: TQuickDownload;
begin
  downloader := TQuickDownload.Create(True);
  downloader.URL := SnapPackages.PackageList[IconIndex].icon;
  downloader.OnFileFinished := @OnDownloadComplete;
  downloader.OnException := @OnDownloadException;
  downloader.Start;
end;

procedure TAppCentreForm.AddDefaultIcon;
var
  pngimg: TPortableNetworkGraphic;
  path: string;
begin
  path := ExtractFilePath(Application.ExeName);
  path := IncludeTrailingPathDelimiter(path + '../');
  path := ExpandFileName(path) + 'share/kirana-appcentre/icons/kirana-brokenimg.png';
  pngimg := TPortableNetworkGraphic.Create;
  pngimg.LoadFromFile(path);
  PackageIcons.Add(pngimg, nil);
end;

procedure TAppCentreForm.OnDownloadException(Sender: TObject);
begin
  AddDefaultIcon;
  LvPackages.Items[IconIndex].ImageIndex := IconIndex +1;
  LvPackages.Invalidate;
  if IconIndex +1 < SnapPackages.PackageList.Count then
  begin
    IconIndex += 1;
    GetPackageIcon;
    LbStatus.Caption := 'Loading ' + IconIndex.ToString
      + ' of ' + SnapPackages.PackageList.Count.ToString + ' package(s)...';
  end
  else
  begin
    LbStatus.Caption := SnapPackages.PackageList.Count.ToString + ' package(s) loaded.';
  end;
end;

procedure TAppCentreForm.OnDownloadComplete(Sender: TObject);
var
  pngimg: TPortableNetworkGraphic;
begin
  pngimg := TPortableNetworkGraphic.Create;
  TQuickDownload(Sender).Data.Position := 0;
  pngimg.LoadFromStream(TQuickDownload(Sender).Data);
  PackageIcons.Add(pngimg, nil);

  LvPackages.Items[IconIndex].ImageIndex := IconIndex +1;
  LvPackages.Invalidate;
  if IconIndex +1 < SnapPackages.PackageList.Count then
  begin
    IconIndex += 1;
    GetPackageIcon;
    LbStatus.Caption := 'Loading ' + IconIndex.ToString
      + ' of ' + SnapPackages.PackageList.Count.ToString + ' package(s)...';
  end
  else
  begin
    LbStatus.Caption := SnapPackages.PackageList.Count.ToString + ' package(s) loaded.';
  end;

end;

procedure TAppCentreForm.FormShow(Sender: TObject);
var
  i: integer;
  item: TListItem;
begin
  IconIndex := 0;
  PackageIcons.Clear;
  AddDefaultIcon;

  for i := 0 to SnapPackages.PackageList.Count -1 do
  begin
    item := LvPackages.Items.Add;
    item.Caption := SnapPackages.PackageList[i].title;
    //item.ImageIndex := 0;
  end;

  if SnapPackages.PackageList.Count > 0 then
    GetPackageIcon;
end;

procedure TAppCentreForm.LvPackagesSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
var
  i: integer;
begin
  for i := 0 to LvPackages.Items.Count -1 do
  begin
    if LvPackages.Items[i].Caption = SnapPackages.PackageList[i].title then
    begin
      LvPackages.Items[i].Caption := OptimizeTitleDisplay(SnapPackages.PackageList[i].title);
    end;
    if  LvPackages.Items[i].Selected then
      LvPackages.Items[i].Caption := SnapPackages.PackageList[i].title;
  end;

end;

procedure TAppCentreForm.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  Application.Terminate;
end;

procedure TAppCentreForm.Button2Click(Sender: TObject);
begin

end;

end.

