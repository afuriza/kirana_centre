unit SnapUtils;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, ShellCommandRunner, fpjson, jsonparser, fgl;

function BufferToString(const pBuffer: PByteArray; const pCount: integer): string;


const
  SNAPV2API = 'curl -sS --unix-socket /run/snapd.socket http://localhost/v2/';
  URI_FIND = 'find';


type
  TPublisher = class(TObject)
    id: string;
    username: string;
    display_name: string;
    channel: string;
  end;

  TMediaData = class(TObject)
    &type: string;
    url: string;
    Width: integer;
    Height: integer;
  end;

  TMedia = specialize TFPGList<TMediaData>;

  TPackage = class(TObject)
    id: string;
    title: string;
    summary: string;
    description: string;
    downloadsize: int64;
    icon: string;
    Name: string;
    publisher: TPublisher;
    store_url: string;
    developer: string;
    status: string;
    &type: string;
    base: string;
    version: string;
    channel: string;
    ignore_validation: boolean;
    revision: string;
    confinement: string;
    &private: boolean;
    devmode: boolean;
    jailmode: boolean;
    contact: string;
    license: string;
    website: string;
    media: TMedia;
  end;

  TPackageList = specialize TFPGList<TPackage>;

  TSnapPackages = class(TComponent)
  private
    fOnTaskDone: TNotifyEvent;
    procedure OnOutputAvailable(const pBuffer: PByteArray; const pCount: integer);
    procedure OnOutputEnd(Sender: TObject);
  public
    ResponseData: TStringList;
    PackageList: TPackageList;
    Server: TShellCommandRunnerThread;
    procedure InitServer;
    procedure StartTask(AUri: string);
    procedure StartTask(AUri: string; AParam: array of string);
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property OnTaskDone: TNotifyEvent read fOnTaskDone write fOnTaskDone;
  end;

var
  SnapPackages: TSnapPackages;

implementation

function BufferToString(const pBuffer: PByteArray; const pCount: integer): string;
begin
  Result := TShellCommandRunner.BufferToString(pBuffer, pCount);
end;

constructor TSnapPackages.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  PackageList := TPackageList.Create;
  ResponseData := TStringList.Create;
end;

destructor TSnapPackages.Destroy;
begin
  FreeAndNil(ResponseData);
  FreeAndNil(PackageList);
  inherited Destroy;
end;

procedure TSnapPackages.OnOutputEnd(Sender: TObject);
var
  jData: TJsonObject;
  jPackages: TJsonArray;
  jPackage: TJsonObject;
  jMedia: TJsonObject;
  jPublisher: TJsonObject;
  Package: TPackage;
  MediaData: TMediaData;
  Publisher: TPublisher;
  i, j, k, l: integer;
  path: string;
begin
  //jData := TJsonObject(GetJSON(ResponseData.Text));
  path := ExtractFilePath(paramstr(0));
  path := IncludeTrailingPathDelimiter(path + '../');
  path := ExpandFileName(path) + 'share/kirana-appcentre/test.json';
  ResponseData.LoadFromFile(path);
  jData := TJsonObject(GetJSON(ResponseData.Text));
  if jData.FindPath('status-code').AsInteger = 200 then
  begin
    jPackages := TJSONArray(jData.FindPath('result'));
    for i := 0 to jPackages.Count - 1 do
    begin
      jPackage := TJsonObject(jPackages[i]);
      Package := TPackage.Create;
      for j := 0 to jPackage.Count - 1 do
      begin
        case jPackage.Names[j] of
          'id': Package.id := jPackage.FindPath('id').AsString;
          'title': Package.title := jPackage.FindPath('title').AsString;
          'summary': Package.summary := jPackage.FindPath('summary').AsString;
          'version': Package.version := jPackage.FindPath('version').AsString;
          'base': Package.base := jPackage.FindPath('base').AsString;
          'channel': Package.channel := jPackage.FindPath('channel').AsString;
          'confinement': Package.confinement :=
              jPackage.FindPath('confinement').AsString;
          'contact': Package.contact := jPackage.FindPath('contact').AsString;
          'description': Package.description :=
              jPackage.FindPath('description').AsString;
          'developer': Package.developer := jPackage.FindPath('developer').AsString;
          'devmode': Package.devmode := jPackage.FindPath('devmode').AsBoolean;
          'downloadsize': Package.downloadsize :=
              jPackage.FindPath('downloadsize').AsInt64;
          'icon': Package.icon := jPackage.FindPath('icon').AsString;
          'ignore-validation':
            Package.ignore_validation :=
              jPackage.FindPath('ignore-validation').AsBoolean;
          'jailmode': Package.jailmode := jPackage.FindPath('jailmode').AsBoolean;
          'license': Package.license := jPackage.FindPath('license').AsString;
          'media':
          begin
            Package.media := TMedia.Create;
            for k := 0 to TJSONArray(jPackages[i].FindPath('media')).Count - 1 do
            begin
              jMedia := TJsonObject(TJSONArray(jPackages[i].FindPath('media'))[k]);
              MediaData := TMediaData.Create;
              for l := 0 to jMedia.Count - 1 do
              begin
                case jMedia.Names[l] of
                  'url': MediaData.url := jMedia.FindPath('url').AsString;
                  'type': MediaData.&type := jMedia.FindPath('type').AsString;
                  'width': MediaData.Width := jMedia.FindPath('width').AsInteger;
                  'height': MediaData.Height := jMedia.FindPath('height').AsInteger;
                end;
              end;
              Package.media.Add(MediaData);
            end;
          end;
          'publisher':
          begin
            Package.publisher := TPublisher.Create;
            jPublisher := TJsonObject(jPackages[i].FindPath('publisher'));
            for k := 0 to jPublisher.Count - 1 do
            begin
              case jPublisher.Names[k] of
                'id': Package.publisher.id := jPublisher.FindPath('id').AsString;
                'username': Package.publisher.username := jPublisher.FindPath('username').AsString;
                'display-name': Package.publisher.display_name := jPublisher.FindPath('display-name').AsString;
                'channel': Package.publisher.channel := jPublisher.FindPath('channel').AsString;
              end;
            end;
          end;
        end;
      end;
      PackageList.Add(Package);
    end;
  end;
  fOnTaskDone(Sender);
end;

procedure TSnapPackages.OnOutputAvailable(const pBuffer: PByteArray;
  const pCount: integer);
begin
  ResponseData.Append(BufferToString(pBuffer, pCount));
end;

procedure TSnapPackages.InitServer;
begin
  Server := TShellCommandRunnerThread.Create;
end;

procedure TSnapPackages.StartTask(AUri: string);
begin
  StartTask(AUri, []);
end;

procedure TSnapPackages.StartTask(AUri: string; AParam: array of string);
begin
  InitServer;
  Server.CommandLine := SNAPV2API + AUri;
  Server.OnOutputAvailable := @OnOutputAvailable;
  Server.OnTerminate := @OnOutputEnd;
  Server.Start;
end;

initialization
  SnapPackages := TSnapPackages.Create(nil);

finalization
  FreeAndNil(SnapPackages);

end.
