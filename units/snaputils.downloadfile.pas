unit SnapUtils.DownloadFile;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, fphttpclient, opensslsockets;

  function OptimizeTitleDisplay(ATitle: string): string;

type

  TOnWriteStream = procedure(Sender: TObject; APos: Int64) of object;
  TDownloadStream = class(TStream)
  private
    FOnWriteStream: TOnWriteStream;
    FStream: TStream;
  public
    constructor Create(AStream: TStream);
    destructor Destroy; override;
    function Read(var Buffer; Count: LongInt): LongInt; override;
    function Write(const Buffer; Count: LongInt): LongInt; override;
    function Seek(Offset: LongInt; Origin: Word): LongInt; override;
    procedure DoProgress;
  published
    property OnWriteStream: TOnWriteStream read FOnWriteStream write FOnWriteStream;
  end;

  TQuickDownload = class(TThread)
  protected
    procedure Execute; override;
  private
    //procedure Ready;
    fOnFileFinished: TNotifyEvent;
    fOnException: TNotifyEvent;
  public
    Data: TMemoryStream;
    URL: string;
    property OnFileFinished: TNotifyEvent read fOnFileFinished write fOnFileFinished;
    property OnException: TNotifyEvent read fOnException write fOnException;
    constructor Create(CreateSuspended: Boolean;
      const StackSize: SizeUInt = DefaultStackSize);
  end;

implementation

{ TDownloadStream }

function OptimizeTitleDisplay(ATitle: string): string;
var
  SL: TStringList;
  Disable2ndLine: Boolean;
begin
  Disable2ndLine := False;
  SL := TStringList.Create;
  SL.Delimiter := ' ';
  SL.DelimitedText := ATitle;
  if SL.Count > 0 then
  begin
    if SL[0].Length > 7 then
    begin
      Disable2ndLine := True;
      Result := Copy(SL[0], 1, 7) + '...';
    end
    else
    begin
      Result := SL[0];
    end;
  end;
  if (SL.Count > 1) and (not Disable2ndLine) then
  begin
    if SL[1].Length > 7 then
    begin
      Result := Result + ' ' + Copy(SL[1], 1, 7) + '...';
    end
    else
    begin
      Result := Result + ' ' + SL[1];
    end;
  end;
  FreeAndNil(SL);
end;

constructor TDownloadStream.Create(AStream: TStream);
begin
  inherited Create;
  FStream := AStream;
  FStream.Position := 0;
end;

destructor TDownloadStream.Destroy;
begin
  FStream.Free;
  inherited Destroy;
end;

function TDownloadStream.Read(var Buffer; Count: LongInt): LongInt;
begin
  Result := FStream.Read(Buffer, Count);
end;

function TDownloadStream.Write(const Buffer; Count: LongInt): LongInt;
begin
  Result := FStream.Write(Buffer, Count);
  DoProgress;
end;

function TDownloadStream.Seek(Offset: LongInt; Origin: Word): LongInt;
begin
  Result := FStream.Seek(Offset, Origin);
end;

procedure TDownloadStream.DoProgress;
begin
  if Assigned(FOnWriteStream) then
    FOnWriteStream(Self, Self.Position);
end;

constructor TQuickDownload.Create(CreateSuspended: Boolean;
  const StackSize: SizeUInt = DefaultStackSize);
begin
  inherited Create(CreateSuspended, StackSize);
end;

procedure TQuickDownload.Execute;
var
  HttpClient: TFPHTTPClient;
begin
  HttpClient := TFPHTTPClient.Create(nil);
  Data := TMemoryStream.Create;
  try
    try
      HttpClient.HTTPMethod('GET', URL, Data, [200]);
      if Assigned(fOnFileFinished) then
        fOnFileFinished(Self);
    except
      on E: Exception do
      begin
        if Assigned(fOnException) then
          fOnException(self);
      end;
    end;
  finally
    Data.Free;
    HttpClient.Free;
  end;
end;

end.

